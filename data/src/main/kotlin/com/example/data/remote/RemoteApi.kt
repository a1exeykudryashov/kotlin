package com.example.data.remote

import com.example.data.remote.apimodels.UsersResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteApi {
    @GET("search/users")
    fun getUsers(@Query("q") query: String): Single<UsersResponse>
}