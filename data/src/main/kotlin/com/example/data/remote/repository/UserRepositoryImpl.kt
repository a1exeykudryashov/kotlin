package com.example.data.remote.repository

import com.example.data.remote.RemoteApi
import com.example.data.remote.apimodels.User
import com.example.domain.model.UserViewModel
import com.example.domain.repository.UserRepository
import io.reactivex.Single

class UserRepositoryImpl(private val api: RemoteApi) : UserRepository {
    override fun searchUsers(query: String): Single<List<UserViewModel>> {
        return api.getUsers(query)
                .map { response ->
                    response.items.map { user ->
                        userViewModelMapper(user)
                    }
                }
    }

    private fun userViewModelMapper(user: User) = UserViewModel(user.id, user.login)
}