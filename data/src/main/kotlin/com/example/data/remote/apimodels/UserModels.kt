package com.example.data.remote.apimodels

import com.google.gson.annotations.SerializedName

data class User(
        @SerializedName("id") val id: Long,
        @SerializedName("login") val login: String)

data class UsersResponse(
        @SerializedName("items") val items: List<User>
)