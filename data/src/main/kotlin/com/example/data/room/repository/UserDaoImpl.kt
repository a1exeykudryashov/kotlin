package com.example.data.room.repository

import com.example.data.room.dao.RoomUserDao
import com.example.data.room.entities.UserEntity
import com.example.domain.model.UserViewModel
import com.example.domain.repository.UserDao
import io.reactivex.Completable
import io.reactivex.Single

class UserDaoImpl(private val roomUserDao: RoomUserDao) : UserDao {

    override fun getAll(): Single<List<UserViewModel>> {
        return roomUserDao.getAll()
                .map { userEntities ->
                    userEntities.map { userEntity ->
                        userViewModelMapper(userEntity)
                    }
                }
    }

    override fun insertAll(list: List<UserViewModel>): Completable {
        return Completable.fromAction {
            val userEntities = list.mapIndexed { index, userViewModel ->
                userEntityMapper(index, userViewModel)
            }
            roomUserDao.insertAll(userEntities)
        }
    }

    override fun deleteAll(): Completable = Completable.fromAction {
        roomUserDao.deleteAll()
    }

    private fun userViewModelMapper(userEntity: UserEntity) = UserViewModel(userEntity.id, userEntity.login)
    private fun userEntityMapper(index: Int, userViewModel: UserViewModel) = UserEntity(userViewModel.id, userViewModel.login, index)
}