package com.example.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.room.entities.UserEntity
import io.reactivex.Single

@Dao
interface RoomUserDao {
    @Query("SELECT * FROM user ORDER BY user.ord")
    fun getAll(): Single<List<UserEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(list: List<UserEntity>)

    @Query("DELETE FROM user")
    fun deleteAll()
}