package com.example.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.domain.interactors.UsersInteractor
import com.example.domain.model.UserViewModel
import com.example.domain.repository.UserDao
import com.example.domain.repository.UserRepository
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class UserUserCasesTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var userRepository: UserRepository

    @Mock
    private lateinit var userDao: UserDao

    private val usersUseCases by lazy { UsersInteractor(userRepository, userDao) }

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this@UserUserCasesTest)
    }

    @Test
    fun testUsersUseCases_searchUsersBy_Completed() {
        val response = emptyList<UserViewModel>()
        whenever(userRepository.searchUsers(anyString()))
                .thenReturn(Single.just(response))

        whenever(userDao.deleteAll())
                .thenReturn(Completable.complete())

        whenever(userDao.insertAll(response))
                .thenReturn(Completable.complete())

        usersUseCases.searchAndSaveUsersBy(ListFixture.QUERY)
                .test()
                .assertComplete()
    }

    @Test
    fun testUsersUseCases_searchUsersBy_Error() {
        val response = Throwable(ListFixture.ERROR)
        whenever(userRepository.searchUsers(anyString()))
                .thenReturn(Single.error(response))

        usersUseCases.searchAndSaveUsersBy(ListFixture.QUERY)
                .test()
                .assertError(response)
    }

    @Test
    fun testUsersUseCases_searchUsersBy_Response() {
        val response = ListFixture.getRemoteUserViewModels()
        whenever(userRepository.searchUsers(ListFixture.QUERY))
                .thenReturn(Single.just(response))

        whenever(userDao.deleteAll())
                .thenReturn(Completable.complete())

        whenever(userDao.insertAll(response))
                .thenReturn(Completable.complete())

        usersUseCases.searchAndSaveUsersBy(ListFixture.QUERY)
                .test()
                .assertValue(response)
    }
}