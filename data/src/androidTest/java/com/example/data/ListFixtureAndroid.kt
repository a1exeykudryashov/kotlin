package com.example.data

import com.example.data.room.entities.UserEntity

object ListFixtureAndroid {

    private const val TEST_USER_ID_1 = 1L
    private const val TEST_USER_ID_2 = 2L
    private const val TEST_USER_LOGIN_1 = "user1"
    private const val TEST_USER_LOGIN_2 = "user2"

    fun getUserEntities() = listOf(
            UserEntity(TEST_USER_ID_1, TEST_USER_LOGIN_1, 0),
            UserEntity(TEST_USER_ID_2, TEST_USER_LOGIN_2, 1)
    )
}