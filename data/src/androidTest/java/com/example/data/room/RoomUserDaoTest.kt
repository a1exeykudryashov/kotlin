package com.example.data.room

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.data.ListFixtureAndroid
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
open class RoomUserDaoTest : DbTest() {
    @Test
    fun whenInsertAllThenReadThem() {
        val users = ListFixtureAndroid.getUserEntities()
        appDatabase.userDao().insertAll(users)
        appDatabase.userDao().getAll()
                .test()
                .assertValue { list -> list.size == users.size }
    }

    @Test
    fun whenDeleteAllThenReadNothing() {
        appDatabase.userDao().deleteAll()
        appDatabase.userDao().getAll()
                .test()
                .assertValue { list -> list.isEmpty() }
    }
}
