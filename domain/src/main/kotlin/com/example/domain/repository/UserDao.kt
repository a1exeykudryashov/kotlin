package com.example.domain.repository

import com.example.domain.model.UserViewModel
import io.reactivex.Completable
import io.reactivex.Single

interface UserDao {
    fun getAll(): Single<List<UserViewModel>>
    fun insertAll(list: List<UserViewModel>): Completable
    fun deleteAll(): Completable
}