package com.example.domain.repository

import com.example.domain.model.UserViewModel
import io.reactivex.Single

interface UserRepository {
    fun searchUsers(query: String): Single<List<UserViewModel>>
}