package com.example.domain.model

data class UserViewModel(val id: Long, val login: String) {
    override fun toString() = login
}