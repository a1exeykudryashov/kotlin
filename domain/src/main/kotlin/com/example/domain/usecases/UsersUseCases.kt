package com.example.domain.usecases

import com.example.domain.model.UserViewModel
import io.reactivex.Single

interface UsersUseCases {
    fun searchAndSaveUsersBy(query: String): Single<List<UserViewModel>>
    fun getUsersFromDb(): Single<List<UserViewModel>>
}

