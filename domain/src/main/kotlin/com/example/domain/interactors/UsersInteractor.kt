package com.example.domain.interactors

import com.example.domain.model.UserViewModel
import com.example.domain.repository.UserDao
import com.example.domain.repository.UserRepository
import com.example.domain.usecases.UsersUseCases
import io.reactivex.Single

class UsersInteractor(private val userRepository: UserRepository, private val userDao: UserDao) : UsersUseCases {
    override fun searchAndSaveUsersBy(query: String): Single<List<UserViewModel>> {
        return userRepository.searchUsers(query)
                .flatMap { users ->
                    userDao.deleteAll()
                            .andThen(userDao.insertAll(users))
                            .toSingle { users }
                }
    }

    override fun getUsersFromDb(): Single<List<UserViewModel>> = userDao.getAll()
}