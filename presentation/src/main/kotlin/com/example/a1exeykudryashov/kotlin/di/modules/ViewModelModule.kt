package com.example.a1exeykudryashov.kotlin.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.a1exeykudryashov.kotlin.viewmodel.base.ViewModelFactory
import com.example.a1exeykudryashov.kotlin.viewmodel.base.ViewModelKey
import com.example.a1exeykudryashov.kotlin.viewmodel.UsersViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(UsersViewModel::class)
    abstract fun bindUsersViewModel(usersViewModel: UsersViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}