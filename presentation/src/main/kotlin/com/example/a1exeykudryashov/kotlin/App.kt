package com.example.a1exeykudryashov.kotlin

import android.app.Application
import com.example.a1exeykudryashov.kotlin.di.AppComponent
import com.example.data.BuildConfig.API_URL
import com.squareup.leakcanary.LeakCanary
import timber.log.Timber

class App : Application() {
    companion object {
        @JvmStatic
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        appComponent = AppComponent.create(this, API_URL)

        Timber.plant(Timber.DebugTree())

        if (LeakCanary.isInAnalyzerProcess(this)) return
        LeakCanary.install(this)
    }
}