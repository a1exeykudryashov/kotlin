package com.example.a1exeykudryashov.kotlin.di

import android.app.Application
import com.example.a1exeykudryashov.kotlin.di.modules.*
import com.example.a1exeykudryashov.kotlin.view.BaseActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            ApiModule::class,
            AppModule::class,
            DaoModule::class,
            NetModule::class,
            RepositoryModule::class,
            UseCasesModule::class,
            ViewModelModule::class
        ]
)

interface AppComponent {
    fun inject(baseActivity: BaseActivity)

    companion object Factory {
        fun create(app: Application, baseUrl: String): AppComponent {
            return DaggerAppComponent.builder()
                    .appModule(AppModule(app))
                    .apiModule(ApiModule())
                    .netModule(NetModule(baseUrl))
                    .repositoryModule(RepositoryModule())
                    .daoModule(DaoModule())
                    .build()
        }
    }
}