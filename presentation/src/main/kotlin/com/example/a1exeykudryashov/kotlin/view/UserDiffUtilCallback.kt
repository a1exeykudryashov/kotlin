package com.example.a1exeykudryashov.kotlin.view

import androidx.recyclerview.widget.DiffUtil
import com.example.domain.model.UserViewModel

class UserDiffUtilCallback(private val oldList: List<UserViewModel>,
                           private val newList: List<UserViewModel>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldProduct = oldList[oldItemPosition]
        val newProduct = newList[newItemPosition]
        return oldProduct.id == newProduct.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldProduct = oldList[oldItemPosition]
        val newProduct = newList[newItemPosition]
        return oldProduct.login == newProduct.login
    }
}