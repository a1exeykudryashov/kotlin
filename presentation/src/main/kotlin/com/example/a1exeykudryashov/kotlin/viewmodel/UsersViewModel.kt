package com.example.a1exeykudryashov.kotlin.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import com.example.a1exeykudryashov.kotlin.SchedulersFactoryContract
import com.example.a1exeykudryashov.kotlin.view.MainActivity
import com.example.a1exeykudryashov.kotlin.view.UserDiffUtilCallback
import com.example.a1exeykudryashov.kotlin.viewmodel.base.SingleLiveEvent
import com.example.domain.model.UserViewModel
import com.example.domain.usecases.UsersUseCases
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import java.util.Collections.emptyList
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class UsersViewModel @Inject constructor(
        private val usersUseCases: UsersUseCases,
        private val schedulers: SchedulersFactoryContract
) : ViewModel() {

    companion object {
        const val DEBOUNCE_TIMEOUT = 500L
        fun create(activity: MainActivity) =
                ViewModelProviders.of(activity, activity.viewModelFactory).get(UsersViewModel::class.java)
    }

    val userLoginList = ArrayList<UserViewModel>()

    val refreshUsers = SingleLiveEvent<DiffUtil.DiffResult>()
    val showError = SingleLiveEvent<String>()
    val showProgress = MutableLiveData<Boolean>()

    @Inject
    fun init() {
        configureSearch()
    }

    fun search(query: String) {
        searchSubject.accept(query.trim())
    }

    private val searchSubject = PublishRelay.create<String>()
    private val resultSubject = PublishSubject.create<List<UserViewModel>>()
    private val compositeDisposable = CompositeDisposable()

    private fun configureSearch() {
        val initialResult = Pair<List<UserViewModel>, DiffUtil.DiffResult?>(emptyList(), null)
        compositeDisposable.apply {
            add(resultSubject
                    .scan(initialResult, BiFunction { oldResult, nextUsers ->
                        return@BiFunction Pair(nextUsers, DiffUtil.calculateDiff(UserDiffUtilCallback(oldResult.first, nextUsers)))
                    })
                    .skip(1)
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
                    .subscribe { result ->
                        userLoginList.clear()
                        userLoginList.addAll(result.first)
                        refreshUsers.value = result.second
                    }
            )
            add(usersUseCases.getUsersFromDb()
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
                    .subscribe { users ->
                        resultSubject.onNext(users)
                    }
            )
            add(searchSubject
                    .debounce(DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS, schedulers.io())
                    .distinctUntilChanged()
                    .filter { query ->
                        !query.isEmpty()
                    }
                    .observeOn(schedulers.ui())
                    .doOnNext { showProgress.value = true }
                    .observeOn(schedulers.io())
                    .switchMap { query ->
                        usersUseCases.searchAndSaveUsersBy(query).toObservable()
                    }
                    .observeOn(schedulers.ui())
                    .doOnEach { showProgress.value = false }
                    .doOnError { showError.value = it.message }
                    .retry()
                    .subscribe { users ->
                        resultSubject.onNext(users)
                    }
            )
        }
    }

    override fun onCleared() {
        compositeDisposable.dispose()
    }
}