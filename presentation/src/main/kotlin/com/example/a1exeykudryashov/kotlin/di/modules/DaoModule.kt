package com.example.a1exeykudryashov.kotlin.di.modules

import android.app.Application
import androidx.room.Room
import com.example.data.room.AppDatabase
import com.example.data.room.repository.UserDaoImpl
import com.example.domain.repository.UserDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DaoModule {
    @Provides
    @Singleton
    fun provideAppDatabase(context: Application): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, "AppDatabase").build()

    @Provides
    @Singleton
    fun provideUserDao(db: AppDatabase): UserDao = UserDaoImpl(db.userDao())
}