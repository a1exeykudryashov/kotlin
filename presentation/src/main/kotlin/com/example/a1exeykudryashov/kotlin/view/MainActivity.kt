package com.example.a1exeykudryashov.kotlin.view

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a1exeykudryashov.kotlin.R
import com.example.a1exeykudryashov.kotlin.afterTextChanged
import com.example.a1exeykudryashov.kotlin.viewmodel.UsersViewModel
import com.example.a1exeykudryashov.kotlin.viewmodel.base.nonNullObserve
import kotlinx.android.synthetic.main.a_main.*
import org.jetbrains.anko.longToast

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_main)

        val usersViewModel = UsersViewModel.create(this)

        val viewManager = LinearLayoutManager(this)
        val userAdapter = UserAdapter(usersViewModel.userLoginList)
        rvUsers.apply {
            layoutManager = viewManager
            adapter = userAdapter
        }

        etUsers.afterTextChanged { query ->
            usersViewModel.search(query)
        }

        usersViewModel.refreshUsers.nonNullObserve(this) { result ->
            result.dispatchUpdatesTo(userAdapter)
        }

        usersViewModel.showProgress.nonNullObserve(this) { visible ->
            pbUsers.visibility = if (visible) View.VISIBLE else View.GONE
        }

        usersViewModel.showError.nonNullObserve(this) { message ->
            longToast("Error: $message")
        }
    }
}
