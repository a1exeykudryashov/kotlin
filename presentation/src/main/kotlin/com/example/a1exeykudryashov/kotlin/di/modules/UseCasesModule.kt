package com.example.a1exeykudryashov.kotlin.di.modules

import com.example.domain.interactors.UsersInteractor
import com.example.domain.repository.UserDao
import com.example.domain.repository.UserRepository
import com.example.domain.usecases.UsersUseCases
import dagger.Module
import dagger.Provides

@Module
class UseCasesModule {
    @Provides
    fun provideUsersUseCases(userRepository: UserRepository, userDao: UserDao): UsersUseCases =
            UsersInteractor(userRepository, userDao)
}