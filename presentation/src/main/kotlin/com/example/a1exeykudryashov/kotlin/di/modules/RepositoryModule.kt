package com.example.a1exeykudryashov.kotlin.di.modules

import com.example.data.remote.RemoteApi
import com.example.data.remote.repository.UserRepositoryImpl
import com.example.domain.repository.UserRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun provideUserRepository(api: RemoteApi): UserRepository = UserRepositoryImpl(api)
}