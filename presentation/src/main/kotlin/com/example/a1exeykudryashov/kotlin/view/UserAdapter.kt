package com.example.a1exeykudryashov.kotlin.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.a1exeykudryashov.kotlin.R
import com.example.domain.model.UserViewModel
import kotlinx.android.synthetic.main.i_user.view.*

class UserAdapter(private val items: MutableList<UserViewModel>) :
        RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.i_user, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val userViewModel = items[position]
        holder.tvUserLogin.text = userViewModel.login
    }

    override fun getItemCount() = items.size

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tvUserLogin: TextView = view.tvUserLogin
    }
}