package com.example.a1exeykudryashov.kotlin

import io.reactivex.Scheduler

interface SchedulersFactoryContract {
    fun io(): Scheduler
    fun ui(): Scheduler
}