package com.example.a1exeykudryashov.kotlin.di.modules

import android.app.Application
import com.example.a1exeykudryashov.kotlin.SchedulersFactory
import com.example.a1exeykudryashov.kotlin.SchedulersFactoryContract
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {
    @Provides
    @Singleton
    fun provideApplication(): Application = application

    @Provides
    @Singleton
    fun schedulersFactory(): SchedulersFactoryContract = SchedulersFactory()
}