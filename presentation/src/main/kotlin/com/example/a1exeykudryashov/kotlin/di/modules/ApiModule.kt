package com.example.a1exeykudryashov.kotlin.di.modules

import com.example.data.remote.RemoteApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApiModule {
    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit): RemoteApi = retrofit.create<RemoteApi>(RemoteApi::class.java)
}