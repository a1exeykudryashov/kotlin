package com.example.a1exeykudryashov.kotlin

import io.reactivex.Scheduler
import io.reactivex.schedulers.TestScheduler

class TestSchedulersFactory : SchedulersFactoryContract {

    val testScheduler = TestScheduler()

    override fun io(): Scheduler = testScheduler
    override fun ui(): Scheduler = testScheduler
}