package com.example.a1exeykudryashov.kotlin.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import com.example.a1exeykudryashov.kotlin.ListFixture
import com.example.a1exeykudryashov.kotlin.TestSchedulersFactory
import com.example.domain.usecases.UsersUseCases
import com.example.a1exeykudryashov.kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.concurrent.TimeUnit

@RunWith(JUnit4::class)
class UsersViewModelTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var usersUseCases: UsersUseCases

    @Mock
    private lateinit var refreshUsersObserver: Observer<DiffUtil.DiffResult>

    @Mock
    private lateinit var showErrorObserver: Observer<String>

    @Mock
    private lateinit var showProgressObserver: Observer<Boolean>

    private var schedulers = TestSchedulersFactory()

    private lateinit var usersViewModel: UsersViewModel

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this@UsersViewModelTest)
        usersViewModel = UsersViewModel(usersUseCases, schedulers)

        usersViewModel.refreshUsers.observeForever(refreshUsersObserver)
        usersViewModel.showError.observeForever(showErrorObserver)
        usersViewModel.showProgress.observeForever(showProgressObserver)
    }

    @Test
    fun testUsersViewModel_init_Complete() {
        val localUsers = ListFixture.getLocalUserViewModels()
        whenever(usersUseCases.getUsersFromDb())
                .thenReturn(Single.just(localUsers))

        assertIsEmptyState()

        usersViewModel.init()
        schedulers.testScheduler.triggerActions()

        verify(usersUseCases).getUsersFromDb()
        assert(usersViewModel.userLoginList == localUsers)
        assert(usersViewModel.showError.value == null)
        assert(usersViewModel.showProgress.value == null)
    }

    @Test
    fun testUsersViewModel_search_Response() {
        val response = ListFixture.getRemoteUserViewModels()
        whenever(usersUseCases.getUsersFromDb())
                .thenReturn(Single.just(ListFixture.getLocalUserViewModels()))
        whenever(usersUseCases.searchAndSaveUsersBy(ListFixture.QUERY))
                .thenReturn(Single.just(response))

        assertIsEmptyState()

        usersViewModel.init()
        usersViewModel.search(ListFixture.QUERY)
        schedulers.testScheduler.advanceTimeBy(UsersViewModel.DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS)

        verify(usersUseCases).searchAndSaveUsersBy(ListFixture.QUERY)
        verify(showProgressObserver).onChanged(true)
        assert(usersViewModel.userLoginList == response)
        assert(usersViewModel.showError.value == null)
        assert(usersViewModel.showProgress.value == false)
    }

    @Test
    fun testUsersViewModel_search_Error() {
        val response = Throwable(ListFixture.ERROR)
        val localUsers = ListFixture.getLocalUserViewModels()
        whenever(usersUseCases.getUsersFromDb())
                .thenReturn(Single.just(localUsers))
        whenever(usersUseCases.searchAndSaveUsersBy(ListFixture.QUERY))
                .thenReturn(Single.error(response))

        assertIsEmptyState()

        usersViewModel.init()
        usersViewModel.search(ListFixture.QUERY)
        schedulers.testScheduler.advanceTimeBy(UsersViewModel.DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS)

        verify(usersUseCases).searchAndSaveUsersBy(ListFixture.QUERY)
        verify(showProgressObserver).onChanged(true)
        assert(usersViewModel.userLoginList == localUsers)
        assert(usersViewModel.showError.value == ListFixture.ERROR)
        assert(usersViewModel.showProgress.value == false)
    }

    private fun assertIsEmptyState() {
        assert(usersViewModel.userLoginList.isEmpty())
        assert(usersViewModel.refreshUsers.value == null)
        assert(usersViewModel.showError.value == null)
        assert(usersViewModel.showProgress.value == null)
    }
}