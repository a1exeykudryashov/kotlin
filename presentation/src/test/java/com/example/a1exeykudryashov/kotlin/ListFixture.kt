package com.example.a1exeykudryashov.kotlin

import com.example.domain.model.UserViewModel

object ListFixture {

    const val QUERY = "user"
    const val ERROR = "error"

    private const val TEST_USER_ID_1 = 1L
    private const val TEST_USER_ID_2 = 2L
    private const val TEST_USER_LOGIN_1 = "user1"
    private const val TEST_USER_LOGIN_2 = "user2"

    fun getRemoteUserViewModels() = listOf(
            UserViewModel(TEST_USER_ID_1, TEST_USER_LOGIN_1),
            UserViewModel(TEST_USER_ID_2, TEST_USER_LOGIN_2)
    )

    fun getLocalUserViewModels() = listOf(
            UserViewModel(TEST_USER_ID_1, TEST_USER_LOGIN_1),
            UserViewModel(TEST_USER_ID_2, TEST_USER_LOGIN_2)
    )
}